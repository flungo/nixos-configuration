# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      # Include common configuration relevant to this host.
      ../common/base.nix
      ../common/autoupgrade.nix
      ../common/hidpi.nix
      ../common/laptop.nix
      ../common/opal-sed.nix
      ../common/networking/wireless/base.nix
      ../common/workstation.nix
      ../common/graphical.nix
      ../common/gaming.nix
      ../common/i3.nix
      ../common/pantheon.nix
      ../common/lightdm.nix
      # Include host specific configuration
      ./filesystems/sshd.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Host specific networking configuration
  networking.hostName = "asus-ux310u";
  networking.wireless.interfaces = [ "wlp2s0" ];

  # Encrypted block devices
  environment.systemPackages = with pkgs; [
    cryptsetup
  ];
  environment.etc.cryptab = {
    text = ''
      sshd UUID=87aa62e2-2a7e-4ca1-bb69-4ef52e0f5024 /ssd/keys/ST2000LX001-1RG174_ZDZ9TNFE.key luks
    '';
    mode = "0444";
  };

  # Device specific X server config
  services.xserver.dpi = 120;
  services.xserver.config = ''
    Section "Device"
      Identifier  "Intel Graphics" 
      Driver      "intel"
      Option      "Backlight"  "intel_backlight"
    EndSection
  '';

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

}


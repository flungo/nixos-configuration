# Filesystems from SSHD which should automount on demand
{ config, lib, pkgs, ... }:

{
  fileSystems =
    let
      makeFileSystem = subvol:
        {
          device = "/dev/disk/by-uuid/6e699d2f-e104-41f6-83a1-a741780bad1e";
          fsType = "btrfs";
          options = [
            "subvol=${subvol}"
            "noauto"
            "x-systemd.automount"
            "x-systemd.idle-timeout=15min"
          ];
        };
    in
      {
        "/sshd" = makeFileSystem "/";
        "/home/fabrizio/downloads" = makeFileSystem "home/fabrizio/downloads";
        "/home/fabrizio/university" = makeFileSystem "home/fabrizio/downloads";
        "/home/fabrizio/VirtualBox VMs" = makeFileSystem "vm/vbox";
      };
}

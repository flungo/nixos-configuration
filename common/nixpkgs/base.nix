# Base configuration for nixpkgs

{ config, pkgs, ... }:

{
  nixpkgs.config.allowUnfree = true;
}


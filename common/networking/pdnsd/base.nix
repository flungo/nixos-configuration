# PDNSD configuration integrating with resolvconf

{ config, pkgs, ... }:

{
  # Install the pdnsd package
  environment.systemPackages = [ pkgs.pdnsd ];

  # Configuration for pdnsd
  environment.etc."pdnsd.conf".text = 
    ''
      global {
        server_ip = 127.0.0.1;
        #run_as="nobody"; # status_ctl doesn't work when running as nobody - no perms to create socket
        status_ctl = on;
        perm_cache = 131072; # 128 MB
        neg_ttl = 10;
        timeout = 10;
        verbosity = 1;
      }

      server {
        label = "quadrature";
        policy = included;
        include = ".london.quadraturecapital.com.";
        ip = 172.16.1.3;
        timeout = 1;
        interval = 5;
        purge_cache = on;
        proxy_only = on;
      }

      server {
        label = "41kja.lungo.co.uk";
        policy = included;
        include = ".41kja.lungo.co.uk.";
        ip = 172.16.128.0;
        timeout = 1;
        interval = 5;
        purge_cache = on;
        proxy_only = on;
      }

      server {
        label = "resolvconf";
        uptest = query;
        query_test_name = ".";
        timeout = 1;
        interval = 5;
        purge_cache = on;
        proxy_only = on;
        file = "/etc/pdnsd-resolv.conf";
      }

      server {
        label = "fallback";
        policy = fqdn_only;
        ip = 8.8.8.8, 8.8.4.4, 1.1.1.1, 1.0.0.1;
        uptest = query;
        query_test_name = ".";
        timeout = 1;
        interval = 5;
        edns_query = on;
        proxy_only = on;
        randomize_servers = on;
      }

      server {
        label = "root-servers";
        policy = fqdn_only;
        root_server = on;
        randomize_servers = on;
        timeout = 5;
      }

      source {
        owner=localhost;
        serve_aliases=on;
        file="/etc/hosts";
      }
    '';

  # Configure a systemd service to start pdnsd
  systemd.services.pdnsd = {
      wantedBy = [ "multi-user.target" ]; 
      after = [ "network.target" ];
      description = "proxy name server";
      serviceConfig = {
        ExecStart = ''${pkgs.pdnsd}/bin/pdnsd -c /etc/pdnsd.conf'';
        CacheDirectory = "pdnsd";
      };
      restartTriggers = [
        config.environment.etc."pdnsd.conf".source
      ];
  };

  # Configure networking modules to use pdnsd
  networking = {
    resolvconf = {
      # Use a local resolver (pdnsd)
      useLocalResolver = true;
      # Add additional configuration to integrate pdnsd with resolvconf
      extraConfig =
        ''
          # Provide global resolvers for pdnsd
          pdnsd_resolv=/etc/pdnsd-resolv.conf
        '';
    };
  };
}
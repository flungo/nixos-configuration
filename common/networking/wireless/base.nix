# Base configuration to be applied to all hosts
#
# networking.wireless.interfaces should be configured to avoid the service randomly failing to start.

{ config, pkgs, ... }:

{
  imports =
    [
      ./networks.nix
    ];

  networking.wireless = {
    enable = true; # Enables wireless support via wpa_supplicant.

    # Allow wpa_cli and wpa_gui to control the wpa_suplicant.
    userControlled.enable = true;

    # Disable the p2p adapters (not used)
    extraConfig = ''
      p2p_disabled=1
    '';
  };

  # Disable NetworkManaged when using networking.wireless
  networking.networkmanager.enable = false;
}


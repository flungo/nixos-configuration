# Configuration for host using OPAL SED Drives

{ config, pkgs, ... }:

{
  imports =
    [
      ./services/logind/hibernate.nix
      ./systemd/targets/maskSuspend.nix
    ];

}


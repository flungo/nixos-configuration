# Configuration to be applied to workstation hosts
#
# Workstation hosts are ones used for interactive work

{ config, pkgs, lib, ... }:

{
  imports =
    [
      ./environment/systemPackages/workstation.nix
      ./environment/openfortivpn.nix
      ./fonts/workstation.nix
      ./hardware/pulseaudio/full.nix
      ./hardware/sane/MFC-9330CDW.nix
      ./hardware/sane/HP-MFP-M140w.nix
      # ./networking/pdnsd/base.nix
      ./services/flatpak/base.nix
      ./services/printing/workstation.nix
      ./users/branislava.nix
      ./virtualisation/docker/base.nix
    ] ++ lib.optionals
      (builtins.pathExists ./services/openvpn/client.nix) [
      ./services/openvpn/client.nix
    ];

  nix.settings.sandbox = true;
}


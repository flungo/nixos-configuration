# Configuration to enable docker

{ config, pkgs, ... }:

{
  # Enable docker
  virtualisation.docker.enable = true;

  # Grant access to users without sudo
  # Assumes that fabrizio is always a user
  users.users.fabrizio.extraGroups = [ "docker" ];
}

# Configuration to be applied to devices with HiDPI displays

{ config, pkgs, lib, ... }:

{
  # bigger tty fonts
  console.font = lib.mkDefault
    "${pkgs.terminus_font}/share/consolefonts/ter-u28n.psf.gz";
  services.xserver.dpi = lib.mkDefault 180;
  environment.variables = {
    GDK_SCALE = "2";
    GDK_DPI_SCALE = "0.5";
    _JAVA_OPTIONS = "-Dsun.java2d.uiScale=2";
  };
}

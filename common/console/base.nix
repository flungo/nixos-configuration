# Base configuration to be applied to all hosts

{ config, pkgs, ... }:

{
  # Customise the console configuration
  console = {
    font = "Lat2-Terminus16";
    keyMap = "uk";
  };
}


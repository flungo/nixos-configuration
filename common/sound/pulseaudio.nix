# Sound configuration to be applied to hosts using pulseaudio

{ config, pkgs, ... }:

{
  sound.enable = true;
  hardware.pulseaudio.enable = true;
}


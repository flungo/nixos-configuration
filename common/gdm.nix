# Configuration to be applied to hosts using the gnome desktop manager

{ config, pkgs, ... }:

{
  imports =
    [
      ./services/xserver/gdm.nix
      ./systemd/tmpfiles/gdm/monitor.nix
    ];
}


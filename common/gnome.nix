# Configuration to be applied to hosts using the gnome desktop manager

{ config, pkgs, ... }:

{
  imports =
    [
      ./environment/gnome/minimal.nix
      ./services/xserver/gnome.nix
    ];
}


# Install OpenFortiVPN and bug fixes.

{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    openfortivpn
  ];

  # Workaround for 'Peer refused to agree to his IP address':
  # - https://github.com/adrienverge/openfortivpn/issues/1076
  environment.etc."ppp/options".text = "ipcp-accept-remote";
}


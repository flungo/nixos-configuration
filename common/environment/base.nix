# Base environment configuration to be applied to all hosts

{ config, pkgs, ... }:

{
  imports =
    [
      ./systemPackages/base.nix
    ];

  # Ensure that the user's .profile is sourced for all login sessions (including graphical)
  environment.loginShellInit = ''
    if [ -e $HOME/.profile ]
    then
      . $HOME/.profile
    fi
  '';
}



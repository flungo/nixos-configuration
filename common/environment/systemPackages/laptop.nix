# Packages to be installed on laptops

{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    acpilight
    light
    wpa_supplicant_gui
  ];
}


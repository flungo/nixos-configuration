# Configuration to be applied to hosts with graphical environments

{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    arandr
    firefox
    gedit
    imagemagick
    libnotify
    pcmanfm
    sxiv
    xarchiver
    xclip
  ];
}


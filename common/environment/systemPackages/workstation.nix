# Packages to be installed on workstation hosts

{ config, pkgs, ... }:

let
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in
{
  environment.systemPackages = with pkgs; [
    bmon
    dos2unix
    gitAndTools.git-extras
    git-crypt
    google-chrome
    gparted
    jetbrains.pycharm-community
    jq
    meld
    moreutils
    unstable.nomachine-client
    openfortivpn
    python310
    python310Packages.pip
    signal-desktop
    vlc
    vscode
    whatsapp-for-linux
    wineWowPackages.stable
    woeusb
    wpsoffice
    zathura
    zoom-us
  ];

  services.teamviewer.enable = true;
}


# Packages to be installed on hosts using the i3 window manager

{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    clipmenu # Clipboard Manager
    dunst # Notification Daemon
    playerctl # Media Controller
    python310Packages.py3status # i3status bar
    ranger # File Manager
    rxvt_unicode # Terminal Emulator
    scrot # Screenshots
    xcwd # Util for getting the cwd of the selected X window
    xpad # Sticky Notes
    zathura # PDF Viewer
  ];
}


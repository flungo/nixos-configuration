# Base packages which are expected on all machines

{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # Core utilities
    silver-searcher
    bind # dig
    file
    lm_sensors
    lsof
    usbutils

    # Editors
    vim

    # Development tools
    git

    # Archive tools
    bzip2
    gnutar
    gzip
    p7zip
    unrar
    zip
    unzip
  ];

  # Make vim the default editor
  environment.variables.EDITOR = "vim";
}


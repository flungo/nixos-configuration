# Base configuration to be applied to all hosts

{ config, pkgs, ... }:

{
  # Set time zone.
  time.timeZone = "Europe/London";
}


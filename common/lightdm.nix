# Configuration to be applied to hosts using lightdm

{ config, pkgs, ... }:

{
  imports =
    [
      ./services/xserver/lightdm.nix
      ./programs/dconf/packages/lightdm.nix
    ];
}


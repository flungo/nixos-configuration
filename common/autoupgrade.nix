# Configure NixOS to automatically update

{ config, pkgs, ... }:

{
  # Configure the nix environment with the current NIXOS_CONFIG
  # This should be stable as this configuration should only be reachable if NIXOS_CONFIG was set
  nix.envVars.NIXOS_CONFIG = builtins.getEnv "NIXOS_CONFIG";

  # Enable auto upgrade
  system.autoUpgrade.enable = true;

  # Enable garbage collection to avoid the disk filling up
  nix.gc = {
    automatic = true;
    dates = "weekly";
    randomizedDelaySec = "6h";
    options = "--delete-older-than 30d";
  };
}

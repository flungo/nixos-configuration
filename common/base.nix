# Base configuration to be applied to all hosts

{ config, pkgs, ... }:

{
  imports =
    [
      ./console/base.nix
      ./environment/base.nix
      ./i18n/base.nix
      ./nixpkgs/base.nix
      ./programs/base.nix
      ./security/wrappers/base.nix
      ./time/base.nix
      ./users/base.nix
    ];

  nix.optimise.automatic = true;
}

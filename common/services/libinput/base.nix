# Libinput configuration

{ config, pkgs, ... }:

{
  # Enable touchpad support.
  services.libinput.enable = true;
  services.libinput.touchpad.accelSpeed = "1";
  services.libinput.touchpad.middleEmulation = true;
  services.libinput.touchpad.naturalScrolling = false;
  services.libinput.touchpad.tapping = true;
}


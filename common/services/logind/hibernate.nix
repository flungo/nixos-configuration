# Hibernate instead of sleep with lid switch trigger.
#
# Useful for hosts with self-encrypting drives where suspend is not supported.

{ config, pkgs, ... }:

{
  services.logind = {
    lidSwitch = "hibernate";
    lidSwitchExternalPower = "hibernate";
  };
}


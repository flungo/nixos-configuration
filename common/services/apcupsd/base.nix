# Enable apcupsd

{ config, pkgs, ... }:

{
  services.apcupsd.enable = true;
  services.apcupsd.configText = ''
    UPSTYPE usb
    NISIP 127.0.0.1
    BATTERYLEVEL 5
    MINUTES 10
    NOLOGON disable
  '';
}


# Enable support for flatpak

{ config, pkgs, lib, ... }:

{
  # To use Flatpak you must enable XDG Desktop Portals
  services.dbus.enable = true;
  xdg.portal = {
    enable = true;
    wlr.enable = true;
    extraPortals = lib.mkDefault [ pkgs.xdg-desktop-portal-gtk ];
  };

  # Enable flatpak
  services.flatpak.enable = true;
}


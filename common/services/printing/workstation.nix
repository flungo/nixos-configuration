# Printer configuration for workstations

{ config, pkgs, ... }:

{
  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Install specific drivers
  services.printing.drivers = [ pkgs.brlaser pkgs.hplip pkgs.hplipWithPlugin ];

  # IPP everywhere capable printer
  services.avahi.enable = true;
  services.avahi.nssmdns4 = true;
  # for a WiFi printer
  services.avahi.openFirewall = true;
}


# X server configuration to be applied to hosts which should use GDM

{ config, pkgs, ... }:

{
  services.xserver = {
    displayManager.gdm.enable = true;
  };
}


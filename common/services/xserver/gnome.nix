# X server configuration to be applied to hosts using the gnome desktop manager

{ config, pkgs, ... }:

{
  services.xserver.desktopManager.gnome.enable = true;
}


# X server configuration to be applied to hosts with graphical environments

{ config, pkgs, ... }:

{
  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.autorun = true;

  # Keyboard configuration
  services.xserver.xkb.layout = "gb";

  # Enable touchpad support.
  services.libinput.enable = true;
  services.libinput.touchpad.accelSpeed = "1";
  services.libinput.touchpad.middleEmulation = true;
  services.libinput.touchpad.naturalScrolling = false;
  services.libinput.touchpad.tapping = true;
}


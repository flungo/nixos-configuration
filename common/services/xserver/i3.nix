# X server configuration to be applied to hosts using the i3 window manager

{ config, pkgs, ... }:

{
  services.xserver = {
    windowManager.i3 = {
      enable = true;
    };
    desktopManager.xterm.enable = false;
  };
}


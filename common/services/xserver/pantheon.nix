# X server configuration to be applied to hosts using the pantheon desktop manager
#
# See https://nixos.org/manual/nixos/stable/index.html#chap-pantheon

{ config, pkgs, ... }:

{
  services.xserver.desktopManager.pantheon.enable = true;
}


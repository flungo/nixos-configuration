# Configure lightdm with the panetheon greeter

{ config, pkgs, lib, ... }:

{
  services.xserver = {
    displayManager.lightdm = {
      enable = true;
      greeters.pantheon.enable = true;
    };

    # Disable VT switching to secure locking
    serverFlagsSection = ''
      Option "DontVTSwitch" "true"
    '';
  };
}


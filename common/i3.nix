# Configuration to be applied to hosts using the i3 window manager

{ config, pkgs, ... }:

{
  imports =
    [
      ./environment/systemPackages/i3.nix
      ./services/xserver/i3.nix
  ];
}


# Program configuration to be applied to laptops

{ config, pkgs, ... }:

{
  # Enable the light program so that perms on the brighness device allow the video group to write
  programs.light.enable = true;
}


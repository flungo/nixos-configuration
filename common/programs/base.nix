# Base configuration to be applied to all hosts
#
# Some programs need SUID wrappers, can be configured further or are
# started in user sessions.

{ config, pkgs, ... }:

{
  programs = {
    dconf.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = false;
    };
    mtr.enable = true;
    ssh = {
      startAgent = true;
      extraConfig = ''
        AddKeysToAgent yes
      '';
    };
  };
}


# Extra dconf configuratgion to improve experience when using lightdm
#
# Makes lightdm the default locker for the <Super>l keybinding using dm-tool.

{ config, pkgs, lib, ... }:

{
  imports =
    [
      ../profile/user.nix
    ];

  programs.dconf.profiles.user.databases = [
    {
      settings = {
        "org/gnome/settings-daemon/plugins/media-keys" = {
          custom-keybindings = [ "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/" ];
        };
        "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
          binding = "<Super>l";
          command = "dm-tool lock";
          name = "LightDM lock";
        };
      };
    }
  ];

  # TODO: Add autostart file for "xautolock -time 1 -locker 'dm-tool lock'"
}


# Create a user (default) profile for dconf

{ config, pkgs, ... }:

{
  imports =
    [
      ../base.nix
    ];

  programs.dconf = {
    enable = true;
    profiles.user = {
      enableUserDb = true;
    };
  };
}


# Base configuration for the dconf program

{ config, pkgs, ... }:

{
  programs.dconf = {
    enable = true;
  };
}


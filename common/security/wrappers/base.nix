# Base configuration to be applied to all hosts

{ config, pkgs, ... }:

{
  security.wrappers = {
    ping = {
      owner = "root";
      group = "root";
      capabilities = "cap_net_raw+p";
      source = "${pkgs.iputils.out}/bin/ping";
    };
  };
}

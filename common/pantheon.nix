# Configuration to be applied to hosts using the pantheon desktop manager

{ config, pkgs, ... }:

{
  imports =
    [
      ./services/xserver/pantheon.nix
    ];
}


# Base configuration to be applied to all hosts

{ config, pkgs, ... }:

{
  # Documentation says to import this, but it doesn't seem to work when included.
  # See: https://github.com/NixOS/nixpkgs/issues/270505
  # imports =
  #   [
  #     <nixpkgs/nixos/modules/services/hardware/sane_extra_backends/brscan4.nix>
  #   ];

  hardware = {
    sane = {
      enable = true;
      brscan4 = {
        enable = true;
        netDevices = {
          home = { model = "MFC-9330CDW"; ip = "172.16.174.19"; };
        };
      };
    };
  };
}


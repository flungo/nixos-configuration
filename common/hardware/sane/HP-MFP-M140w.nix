# Base configuration to be applied to all hosts

{ config, pkgs, ... }:

{
  hardware = {
    sane = {
      enable = true;
      extraBackends = [ pkgs.hplipWithPlugin ];
    };
  };
}


# Hardware support for gaming

{ config, pkgs, ... }:

{
  hardware = {
    steam-hardware.enable = true;
  };
}

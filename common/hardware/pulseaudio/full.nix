# Configuration to be applied to workstation hosts
#
# Workstation hosts are ones used for interactive work

{ config, pkgs, ... }:

{
  imports =
    [
      ./base.nix
    ];

  hardware.pulseaudio = {
    package = pkgs.pulseaudioFull;
    extraConfig =
      "
        # Create a combined sink to standadise the sink output
        load-module module-alsa-sink device=hw:0,0
        load-module module-combine-sink sink_name=combined
        set-default-sink combined
      ";
  };

  environment.systemPackages = with pkgs; [
    pasystray
    pavucontrol
  ];
}


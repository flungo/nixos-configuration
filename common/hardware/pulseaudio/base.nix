# Base configuration for pulseaudio

{ config, pkgs, ... }:

{
  # Enable pulse audio
  hardware.pulseaudio.enable = true;
}


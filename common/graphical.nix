# Configuration to be applied to hosts with graphical environments

{ config, pkgs, ... }:

{
  imports =
    [
      ./environment/systemPackages/graphical.nix
      ./services/libinput/base.nix
      ./services/xserver/base.nix
      ./sound/pulseaudio.nix
    ];
}


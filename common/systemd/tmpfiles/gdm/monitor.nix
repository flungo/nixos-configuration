# Copy the monitors.xml file from Fabrizio's Home directory

{ config, pkgs, ... }:

let
  monitorsXmlContent = builtins.readFile /home/fabrizio/.config/monitors.xml;
  monitorsConfig = pkgs.writeText "gdm_monitors.xml" monitorsXmlContent;
in {
  systemd.tmpfiles.rules = [
    "L+ /run/gdm/.config/monitors.xml - - - - ${monitorsConfig}"
  ];
}

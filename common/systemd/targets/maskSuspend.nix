# Mask the suspend targets

{ config, pkgs, ... }:

{
  systemd.targets.suspend.enable = false;
  systemd.targets.suspend-then-hibernate.enable = false;
  systemd.targets.hybrid-sleep.enable = false;
}


# Configuration to be applied to hosts with graphical environments

{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware/gaming.nix
      ./programs/gaming.nix
    ];
}


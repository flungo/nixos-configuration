# Configuration to be applied to laptops

{ config, pkgs, ... }:

{
  imports =
    [
      ./environment/systemPackages/laptop.nix
      ./programs/laptop.nix
      ./services/upower/laptop.nix
  ];
}
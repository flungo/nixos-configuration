# Base configuration to be applied to all hosts

{ config, pkgs, ... }:

{
  # Create a user for branislava
  users.users.branislava = {
    isNormalUser = true;
    extraGroups = [
      # Allow access to devices
      "audio"
      "video"
    ];
  };
}


# Base configuration to be applied to all hosts

{ config, pkgs, ... }:

{
  # Create a user for myself
  users.users.fabrizio = {
    isNormalUser = true;
    extraGroups = [
      # Enable ‘sudo’ for the user.
      "wheel"
      # Allow access to devices
      "audio"
      "video"
    ];
  };
}


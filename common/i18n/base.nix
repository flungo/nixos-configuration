# Base configuration to be applied to all hosts

{ config, pkgs, ... }:

{
  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_GB.UTF-8";
  };
}

